package exception;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
    public class UnrecognizedPropertyExceptionMapper extends AbstractExceptionMapper<UnrecognizedPropertyException> {

    @Override
    public Response toResponse(UnrecognizedPropertyException e) {
        Response.ResponseBuilder builder = Response.status(Response.Status.BAD_REQUEST)
                .entity(defaultJSON(e))
                .type(MediaType.APPLICATION_JSON);
        return builder.build();
    }
}
