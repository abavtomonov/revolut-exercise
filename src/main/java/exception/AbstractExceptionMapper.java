package exception;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dto.ExceptionDto;

import javax.ws.rs.ext.ExceptionMapper;

abstract class AbstractExceptionMapper<T extends Throwable> implements ExceptionMapper<T> {
    private final ObjectMapper MAPPER = new ObjectMapper().setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

    String defaultJSON(final T exception) {
        exception.printStackTrace();
        ExceptionDto exceptionDto = new ExceptionDto("Error", exception.getClass().getSimpleName(), exception.getMessage(), System.currentTimeMillis());

        try {
            return MAPPER.writeValueAsString(exceptionDto);
        } catch (JsonProcessingException e) {
            return "{\"message\":\"An internal error occurred\"}";
        }
    }
}
