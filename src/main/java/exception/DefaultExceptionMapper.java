package exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
public class DefaultExceptionMapper extends AbstractExceptionMapper<Exception> {
    @Override
    public Response toResponse(Exception ex) {
        Response.ResponseBuilder builder = Response.status(500)
                .entity(defaultJSON(ex))
                .type(MediaType.APPLICATION_JSON);
        return builder.build();
    }


}