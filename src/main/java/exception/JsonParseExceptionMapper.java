package exception;

import com.fasterxml.jackson.core.JsonParseException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
public class JsonParseExceptionMapper extends AbstractExceptionMapper<JsonParseException> {

    @Override
    public Response toResponse(JsonParseException e) {
        Response.ResponseBuilder builder = Response.status(Response.Status.BAD_REQUEST)
                .entity(defaultJSON(e))
                .type(MediaType.APPLICATION_JSON);
        return builder.build();
    }
}
