package exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
public class RuntimeApplicationExceptionMapper extends AbstractExceptionMapper<RuntimeApplicationException> {

    @Override
    public Response toResponse(RuntimeApplicationException ex) {
        Response.ResponseBuilder builder = Response.status(Response.Status.BAD_REQUEST)
                .entity(defaultJSON(ex))
                .type(MediaType.APPLICATION_JSON);
        return builder.build();
    }
}