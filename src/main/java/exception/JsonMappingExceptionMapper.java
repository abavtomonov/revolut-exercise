package exception;

import com.fasterxml.jackson.databind.JsonMappingException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
public class JsonMappingExceptionMapper extends AbstractExceptionMapper<JsonMappingException> {

    @Override
    public Response toResponse(JsonMappingException e) {
        Response.ResponseBuilder builder = Response.status(Response.Status.BAD_REQUEST)
                .entity(defaultJSON(e))
                .type(MediaType.APPLICATION_JSON);
        return builder.build();
    }
}
