package exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
public class WebApplicationExceptionMapper extends AbstractExceptionMapper<WebApplicationException> {
    @Override
    public Response toResponse(WebApplicationException ex) {
        Response.ResponseBuilder builder = Response.fromResponse(ex.getResponse())
                .entity(defaultJSON(ex))
                .type(MediaType.APPLICATION_JSON);
        return builder.build();
    }


}