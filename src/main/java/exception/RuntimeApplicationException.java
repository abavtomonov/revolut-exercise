package exception;

public class RuntimeApplicationException extends RuntimeException {
    public RuntimeApplicationException(String message) {
        super(message);
    }
}
