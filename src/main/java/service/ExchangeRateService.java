package service;

import java.math.BigDecimal;
import java.util.Currency;

public interface ExchangeRateService {
    BigDecimal convert(BigDecimal amount, Currency from, Currency to);
}
