package service;

import dao.ExchangeRateDao;
import utils.Assert;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

public class ExchangeRateServiceImpl implements ExchangeRateService {

    @Inject
    private ExchangeRateDao exchangeRateDao;

    @Override
    public BigDecimal convert(BigDecimal amount, Currency from, Currency to) {
        return getExchangeRate(from, to).multiply(amount).setScale(2, RoundingMode.HALF_UP);
    }

    private BigDecimal getExchangeRate(Currency from, Currency to) {
        BigDecimal rate = exchangeRateDao.getRate(from, to);
        if (rate == null) {
            BigDecimal reverseRate = exchangeRateDao.getRate(to, from);
            Assert.isEntityFound(reverseRate != null, "Could not find exchange rate : {0}-{1}", from.getCurrencyCode(), to.getCurrencyCode());
            rate = BigDecimal.ONE.divide(reverseRate, 4, RoundingMode.HALF_UP);
        }
        return rate;
    }

}
