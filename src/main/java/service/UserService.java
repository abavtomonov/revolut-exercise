package service;

import model.Account;
import model.User;

import java.util.List;

public interface UserService {
    List<Account> findAccountsForUserId(Long id);

    User update(User user);

    User create(User user);

    boolean doesUserExist(Long id);

    User getById(Long id);

    void delete(Long key);
}
