package service;

import dao.AccountDao;
import dao.UserDao;
import model.Account;
import model.User;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static utils.Assert.*;

public class UserServiceImpl implements UserService {
    @Inject
    private UserDao userDao;
    @Inject
    private AccountDao accountDao;

    @Override
    public List<Account> findAccountsForUserId(Long id) {
        Collection<Account> all = accountDao.getAll();
        return all.stream().filter(account -> account.getUserId().equals(id)).collect(Collectors.toList());
    }

    @Override
    public User update(User user) {
        validateUserId(user.getId());
        return userDao.update(user);
    }

    @Override
    public User create(User user) {
        return userDao.create(user);
    }

    @Override
    public boolean doesUserExist(Long id) {
        return userDao.doesUserExist(id);
    }

    @Override
    public User getById(Long id) {
        validateUserId(id);
        return userDao.getById(id);
    }

    @Override
    public void delete(Long key) {
        validateUserId(key);
        userDao.delete(key);
    }

    private void validateUserId(Long id) {
        checkNotNull(id, "Id should not be null");
        checkNaturalNumber(id, "Id should be natural number");
        isEntityFound(userDao.doesUserExist(id), "User {0} not found", id);
    }
}
