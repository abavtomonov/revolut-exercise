package service;

import model.Account;

public interface AccountService {
    Account create(Account account);

    Account update(Account account);

    boolean doesAccountExist(Long id);

    void delete(Long key);
}
