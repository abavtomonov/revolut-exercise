package service;

import dao.TransactionDao;
import model.Transaction;

import javax.inject.Inject;
import java.math.BigDecimal;

import static utils.Assert.checkIsTrue;
import static utils.Assert.checkNaturalNumber;
import static utils.Assert.checkNotNull;

public class TransactionServiceImpl implements TransactionService {

    @Inject
    private TransactionDao transactionDao;

    @Inject
    private AccountService accountService;

    @Override
    public Transaction create(Transaction transaction) {

        validateTransactionPropertiesSetBeforeCreation(transaction);

        return transactionDao.create(transaction);
    }

    @Override
    public Transaction get(Long id) {
        checkNotNull(id, "Transaction id cannot be empty");
        checkNaturalNumber(id, "Transaction id should be natural number");
        return transactionDao.getById(id);
    }

    private void validateTransactionPropertiesSetBeforeCreation(Transaction transaction) {
        Long toAccountId = transaction.getToAccountId();
        Long fromAccountId = transaction.getFromAccountId();

        if (fromAccountId != null)
            checkIsTrue(accountService.doesAccountExist(fromAccountId), "Account {0} does not exist", fromAccountId);
        if (toAccountId != null)
            checkIsTrue(accountService.doesAccountExist(toAccountId), "Account {0} does not exist", toAccountId);

        boolean areAccountsEmpty = fromAccountId != null || toAccountId != null;
        checkIsTrue(areAccountsEmpty, "Either source account or destination account should be set for transaction");

        boolean areAccountsDifferent = fromAccountId == null || toAccountId == null || !fromAccountId.equals(toAccountId);
        checkIsTrue(areAccountsDifferent, "Cannot post transaction for the same account");

        checkNotNull(transaction.getAmount(), "Transaction amount is mandatory");
        checkNotNull(transaction.getCurrency(), "Transaction currency is mandatory");

        boolean isAmountPositive = transaction.getAmount().compareTo(BigDecimal.ZERO) >= 0;
        checkIsTrue(isAmountPositive, "Transaction amount cannot be negative : {0}", transaction.getAmount());
    }

}
