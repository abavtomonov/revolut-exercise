package service;

import model.Transaction;

public interface TransactionService {

    Transaction create(Transaction transaction);

    Transaction get(Long id);
}
