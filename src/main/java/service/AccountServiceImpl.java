package service;

import dao.AccountDao;
import model.Account;
import utils.Assert;

import javax.inject.Inject;

import static utils.Assert.*;

public class AccountServiceImpl implements AccountService {

    @Inject
    private AccountDao accountDao;
    @Inject
    private UserService userService;

    @Override
    public Account create(Account account) {
        validateAccount(account);

        return accountDao.create(account);
    }

    @Override
    public Account update(Account account) {
        validateId(account.getId());
        validateAccount(account);

        return accountDao.update(account);
    }

    @Override
    public boolean doesAccountExist(Long id) {
        return accountDao.doesAccountExist(id);
    }

    @Override
    public void delete(Long key) {
        validateId(key);
        accountDao.delete(key);
    }

    private void validateAccount(Account account) {
        validateUserId(account.getUserId());
        Assert.checkNotNull(account.getCurrency(), "Account currency is mandatory");
    }

    private void validateId(Long id) {
        checkNotNull(id, "Account id cannot be null");
        checkNaturalNumber(id, "Account id should be natural number : {0}", id);
        isEntityFound(accountDao.doesAccountExist(id), "Account {0} not found", id);
    }

    private void validateUserId(Long userId) {
        checkNotNull(userId, "User id cannot be null");
        checkNaturalNumber(userId, "User id should be natural number : {0}", userId);
        checkIsTrue(userService.doesUserExist(userId), "User {0} does not exist", userId);
    }
}
