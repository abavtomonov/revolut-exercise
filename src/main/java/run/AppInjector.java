package run;

import controller.AccountController;
import controller.TransactionController;
import controller.UserController;
import dao.AccountDao;
import dao.ExchangeRateDao;
import dao.TransactionDao;
import dao.UserDao;
import dao.inMemory.AccountDaoImpl;
import dao.inMemory.ExchangeRateDaoImpl;
import dao.inMemory.TransactionDaoImpl;
import dao.inMemory.UserDaoImpl;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import service.*;

import javax.inject.Singleton;

public class AppInjector extends AbstractBinder {
    @Override
    protected void configure() {

        bindToSingleton(AccountDaoImpl.class, AccountDao.class);
        bindToSingleton(ExchangeRateDaoImpl.class, ExchangeRateDao.class);
        bindToSingleton(TransactionDaoImpl.class, TransactionDao.class);
        bindToSingleton(UserDaoImpl.class, UserDao.class);

        bindToSingleton(AccountServiceImpl.class, AccountService.class);
        bindToSingleton(ExchangeRateServiceImpl.class, ExchangeRateService.class);
        bindToSingleton(TransactionServiceImpl.class, TransactionService.class);
        bindToSingleton(UserServiceImpl.class, UserService.class);

        bindAsContract(AccountController.class).in(Singleton.class);
        bindAsContract(UserController.class).in(Singleton.class);
        bindAsContract(TransactionController.class).in(Singleton.class);
    }

    private <T> void bindToSingleton(Class<T> implementation, Class<? super T> contract) {
        bind(implementation).to(contract).in(Singleton.class);
    }
}
