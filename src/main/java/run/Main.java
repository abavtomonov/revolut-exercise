package run;

import com.sun.net.httpserver.HttpServer;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;

public class Main {

    private static final String BASE_URI = "http://localhost/api";
    private static final int PORT = 9998;
    private static URI baseUri;
    private static HttpServer httpServer;

    public static void main(String[] args) throws IOException {
        int port = args.length > 0 ? Integer.valueOf(args[0]) : PORT;
        httpServer = startServer(port);
        System.out.println("The application is ready at " + getWadlUrl());
    }

    public static String getWadlUrl() {
        return baseUri + "/application.wadl";
    }

    public static HttpServer startServer(int port) {
        baseUri = UriBuilder.fromUri(BASE_URI).port(port).build();
        ResourceConfig config = new ResourceConfig()
                .packages("controller", "exception", "model")
                .register(new AppInjector());

        return JdkHttpServerFactory.createHttpServer(baseUri, config);
    }

    public static void shutdownServer() {
        if (httpServer != null) {
            httpServer.stop(0);
        }
    }
}
