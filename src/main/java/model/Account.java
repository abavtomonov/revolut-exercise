package model;

import jersey.repackaged.com.google.common.base.MoreObjects;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.Objects;

public class Account implements CrudModel<Long, Account> {
    private Long id;
    private Long userId;
    private Currency currency;
    private BigDecimal balance;

    public Account() {
    }

    public Account(Long id, Long userId, Currency currency, BigDecimal balance) {
        this.id = id;
        this.userId = userId;
        this.currency = currency;
        setBalance(balance);
    }

    public Account(Long userId, Currency currency, BigDecimal balance) {
        this(null, userId, currency, balance);
    }

    //intercept Jersey initialization
    private void setBalance(BigDecimal balance) {
        this.balance = balance == null ? BigDecimal.ZERO : balance.setScale(2, RoundingMode.HALF_UP);
    }

    public Account(Long id, Account account) {
        this(id, account.getUserId(), account.getCurrency(), account.getBalance());
    }

    public Account(Account account, BigDecimal balance) {
        this(account.getId(), account.getUserId(), account.getCurrency(), balance);
    }

    public Long getId() {
        return id;
    }

    public Currency getCurrency() {
        return currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Long getUserId() {
        return userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public Account newInstance(Long key, Account entity) {
        return new Account(key, entity);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("userId", userId)
                .add("currency", currency)
                .add("balance", balance)
                .toString();
    }
}