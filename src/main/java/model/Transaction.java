package model;

import jersey.repackaged.com.google.common.base.MoreObjects;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.Date;
import java.util.Objects;

public class Transaction {
    private Long id;
    private Long fromAccountId;
    private Long toAccountId;
    private Currency currency;
    private BigDecimal amount;
    private Date executionDate;

    public Transaction() {
    }

    public Transaction(Long id, Long fromAccountId, Long toAccountId, Currency currency, BigDecimal amount, Date executionDate) {
        this.id = id;
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.currency = currency;
        this.amount = amount;
        this.executionDate = executionDate == null ? new Date() : executionDate;
    }

    public Transaction(Long id, Transaction transaction) {
        this(id,
                transaction.getFromAccountId(),
                transaction.getToAccountId(),
                transaction.getCurrency(),
                transaction.getAmount(),
                transaction.getExecutionDate()
        );
    }

    public Transaction(Long fromAccountId, Long toAccountId, Currency currency, BigDecimal amount) {
        this(null, fromAccountId, toAccountId, currency, amount, null);
    }

    //intercept Jersey initialization
    private void setAmount(BigDecimal amount) {
        this.amount = amount.setScale(2, RoundingMode.HALF_UP);
    }

    public Long getId() {
        return id;
    }

    public Long getFromAccountId() {
        return fromAccountId;
    }

    public Long getToAccountId() {
        return toAccountId;
    }

    public Currency getCurrency() {
        return currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Date getExecutionDate() {
        return executionDate;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("fromAccountId", fromAccountId)
                .add("toAccountId", toAccountId)
                .add("currency", currency)
                .add("amount", amount)
                .add("executionDate", executionDate)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
