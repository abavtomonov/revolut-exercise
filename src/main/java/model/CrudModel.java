package model;

public interface CrudModel<K, V> {
    V newInstance(K key, V entity);

    K getId();
}
