package model;

import jersey.repackaged.com.google.common.base.MoreObjects;

import java.util.Objects;

public class User implements CrudModel<Long, User> {
    private Long id;
    private String firstName;
    private String middleName;
    private String lastName;

    public User() {
    }

    public User(Long id, String firstName, String middleName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    public User(String firstName, String middleName, String lastName) {
        this(null, firstName, middleName, lastName);
    }

    public User(Long id, User user) {
        this(id, user.getFirstName(), user.getMiddleName(), user.getLastName());
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public User newInstance(Long key, User user) {
        return new User(key, user);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("firstName", firstName)
                .add("middleName", middleName)
                .add("lastName", lastName)
                .toString();
    }
}
