package dao;

import model.Account;

public interface AccountDao extends CrudDao<Long, Account> {
    void lock(Long accountId);

    void unlock(Long accountId);

    boolean doesAccountExist(Long id);
}
