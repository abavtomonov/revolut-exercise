package dao;

import model.User;

public interface UserDao extends CrudDao<Long, User> {
    boolean doesUserExist(Long id);

}
