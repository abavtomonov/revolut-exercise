package dao;

import model.Transaction;

import java.util.Collection;

public interface TransactionDao {
    Transaction create(Transaction transaction);

    Transaction getById(Long id);

    Collection<Transaction> getAll();
}
