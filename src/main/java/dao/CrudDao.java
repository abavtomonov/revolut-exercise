package dao;

import java.util.Collection;

public interface CrudDao<K, V> {

    V create(V entity);

    V getById(K key);

    Collection<V> getAll();

    V update(V entity);

    void delete(K key);

    void deleteAll();
}
