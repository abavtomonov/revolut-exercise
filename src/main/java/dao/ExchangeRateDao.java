package dao;

import java.math.BigDecimal;
import java.util.Currency;

public interface ExchangeRateDao {
    BigDecimal getRate(Currency from, Currency to);
}
