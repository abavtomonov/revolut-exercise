package dao.inMemory;

import dao.AccountDao;
import exception.RuntimeApplicationException;
import model.Account;
import utils.Assert;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

public class AccountDaoImpl implements AccountDao {
    private static final int SECONDS_TO_WAIT_FOR_ACCOUNT_LOCK = 10;
    private final AtomicLong maxAccountId = new AtomicLong();
    private final Map<Long, ReentrantLock> balanceChangeLockMap = new ConcurrentHashMap<>();
    private Map<Long, Account> storageMap = new ConcurrentHashMap<>();

    @Override
    public Account create(Account entity) {
        Long id = maxAccountId.incrementAndGet();
        Account newAccount = entity.newInstance(id, entity);
        storageMap.put(id, newAccount);
        balanceChangeLockMap.put(id, new ReentrantLock());

        return newAccount;
    }

    @Override
    public Account getById(Long key) {
        Assert.isEntityFound(doesAccountExist(key), "Account {0} not found", key);
        return storageMap.get(key);
    }

    @Override
    public Collection<Account> getAll() {
        return storageMap.values();
    }

    @Override
    public Account update(Account entity) {
        Long id = entity.getId();
        Assert.isEntityFound(doesAccountExist(id), "Account {0} not found", id);
        try {
            lock(id);
            storageMap.put(id, entity);
            return entity;
        } finally {
            unlock(id);
        }
    }

    @Override
    public void delete(Long id) {
        Assert.isEntityFound(doesAccountExist(id), "Account {0} not found", id);
        ReentrantLock lock = null;
        try {
            lock(id);
            storageMap.remove(id);
            lock = balanceChangeLockMap.remove(id);
        } finally {
            if (lock != null) {
                lock.unlock();
            } else {
                unlock(id);
            }
        }
    }

    @Override
    public void deleteAll() {
        storageMap = new ConcurrentHashMap<>();
    }

    @Override
    public void lock(Long accountId) {
        try {
            if (!getLockByAccountId(accountId).tryLock(SECONDS_TO_WAIT_FOR_ACCOUNT_LOCK, TimeUnit.SECONDS)) {
                throw new RuntimeException("Could not acquire lock on account");
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Could not acquire lock on account");
        }
    }

    @Override
    public void unlock(Long accountId) {
        ReentrantLock lockByAccountId = getLockByAccountId(accountId);
        if (lockByAccountId.isHeldByCurrentThread())
            lockByAccountId.unlock();
    }

    @Override
    public boolean doesAccountExist(Long id) {
        return storageMap.containsKey(id);
    }

    private ReentrantLock getLockByAccountId(Long accountId) {
        ReentrantLock lock = balanceChangeLockMap.get(accountId);
        if (lock == null) {
            throw new RuntimeApplicationException("Could not find an account to lock");
        }
        return lock;
    }


}
