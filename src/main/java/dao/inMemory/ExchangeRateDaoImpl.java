package dao.inMemory;

import dao.ExchangeRateDao;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ExchangeRateDaoImpl implements ExchangeRateDao {
    private final Map<String, BigDecimal> storageMap = new ConcurrentHashMap<>();

    {
        storageMap.put("EURUSD", BigDecimal.valueOf(1.1402));
        storageMap.put("EURGBP", BigDecimal.valueOf(0.8072));
        storageMap.put("USDGBP", BigDecimal.valueOf(0.7080));
    }

    @Override
    public BigDecimal getRate(Currency from, Currency to) {
        return storageMap.get(from.getCurrencyCode() + to.getCurrencyCode());
    }
}
