package dao.inMemory;

import dao.UserDao;
import model.User;

import java.util.concurrent.atomic.AtomicLong;

public class UserDaoImpl extends AbstractInMemoryDao<Long, User> implements UserDao {
    private final AtomicLong maxUserId = new AtomicLong();

    @Override
    protected Long getNextId() {
        return maxUserId.incrementAndGet();
    }

    @Override
    public boolean doesUserExist(Long id) {
        return storageMap.containsKey(id);
    }
}
