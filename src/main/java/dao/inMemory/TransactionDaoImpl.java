package dao.inMemory;

import dao.AccountDao;
import dao.TransactionDao;
import model.Account;
import model.Transaction;
import service.ExchangeRateService;
import utils.Assert;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Currency;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class TransactionDaoImpl implements TransactionDao {
    private final AtomicLong maxTransactionId = new AtomicLong();
    private final Map<Long, Transaction> storageMap = new ConcurrentHashMap<>();

    @Inject
    private AccountDao accountDao;

    @Inject
    private ExchangeRateService exchangeRateService;

    public TransactionDaoImpl() {
    }

    @Override
    public Transaction create(Transaction transaction) {

        Long fromAccountId = transaction.getFromAccountId();
        Long toAccountId = transaction.getToAccountId();

        try {
            if (transaction.getFromAccountId() != null)
                accountDao.lock(fromAccountId);
            if (transaction.getToAccountId() != null)
                accountDao.lock(toAccountId);

            return tryToPostTransaction(transaction);
        } finally {
            if (transaction.getFromAccountId() != null)
                accountDao.unlock(fromAccountId);
            if (transaction.getToAccountId() != null)
                accountDao.unlock(toAccountId);
        }
    }

    @Override
    public Transaction getById(Long id) {
        return storageMap.get(id);
    }

    @Override
    public Collection<Transaction> getAll() {
        return storageMap.values();
    }

    private Transaction tryToPostTransaction(Transaction transaction) {
        Currency transactionCurrency = transaction.getCurrency();
        BigDecimal transactionAmount = transaction.getAmount();
        Long fromAccountId = transaction.getFromAccountId();
        Long toAccountId = transaction.getToAccountId();

        if (fromAccountId != null) {
            Account fromAccount = accountDao.getById(fromAccountId);
            Assert.checkNotNull(fromAccount, "Source account {0} could not be found", fromAccountId);
            withdraw(fromAccount, transactionCurrency, transactionAmount);
        }

        if (toAccountId != null) {
            Account toAccount = accountDao.getById(toAccountId);
            Assert.checkNotNull(toAccount, "Destination account {0} could not be found", toAccountId);
            deposit(toAccount, transactionCurrency, transactionAmount);
        }

        Transaction transactionToSave = new Transaction(maxTransactionId.incrementAndGet(), transaction);

        storageMap.put(transactionToSave.getId(), transactionToSave);
        return transactionToSave;
    }

    private void deposit(Account toAccount, Currency transactionCurrency, BigDecimal transactionAmount) {
        BigDecimal amount = getConvertedAmount(toAccount.getCurrency(), transactionCurrency, transactionAmount);
        accountDao.update(new Account(toAccount, toAccount.getBalance().add(amount)));
    }

    private void withdraw(Account fromAccount, Currency transactionCurrency, BigDecimal transactionAmount) {
        BigDecimal amount = getConvertedAmount(fromAccount.getCurrency(), transactionCurrency, transactionAmount);
        Assert.checkIsTrue(fromAccount.getBalance().compareTo(amount) > 0, "Insufficient funds : {0}", fromAccount.getBalance());
        accountDao.update(new Account(fromAccount, fromAccount.getBalance().subtract(amount)));
    }

    private BigDecimal getConvertedAmount(Currency toCurrency, Currency fromCurrency, BigDecimal amount) {
        if (fromCurrency.equals(toCurrency))
            return amount;
        else
            return exchangeRateService.convert(amount, fromCurrency, toCurrency);
    }
}
