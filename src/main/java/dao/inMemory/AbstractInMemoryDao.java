package dao.inMemory;

import dao.CrudDao;
import model.CrudModel;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class AbstractInMemoryDao<K, V extends CrudModel<K, V>> implements CrudDao<K, V> {
    protected Map<K, V> storageMap = new ConcurrentHashMap<>();

    protected abstract K getNextId();

    @Override
    public V create(V entity) {
        K id = getNextId();
        V newAccount = entity.newInstance(id, entity);
        storageMap.put(id, newAccount);
        return newAccount;
    }

    @Override
    public V getById(K key) {
        return storageMap.get(key);
    }

    @Override
    public Collection<V> getAll() {
        return storageMap.values();
    }

    @Override
    public V update(V entity) {
        K id = entity.getId();
        if (id != null) {
            V updatedEntity = storageMap.get(id);
            if (updatedEntity != null) {
                storageMap.put(id, entity);
                return entity;
            }
        }
        return null;
    }

    @Override
    public void delete(K key) {
        storageMap.remove(key);
    }

    @Override
    public void deleteAll() {
        storageMap = new ConcurrentHashMap<>();
    }
}
