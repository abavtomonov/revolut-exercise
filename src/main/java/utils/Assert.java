package utils;

import exception.RuntimeApplicationException;

import javax.ws.rs.NotFoundException;
import java.text.MessageFormat;

public class Assert {

    public static void checkNotNull(Object o, String messagePattern, Object... arguments) {
        if (o == null) {
            exception(messagePattern, arguments);
        }
    }

    public static void checkIsTrue(boolean value, String messagePattern, Object... arguments) {
        if (!value) {
            exception(messagePattern, arguments);
        }
    }

    public static void checkNaturalNumber(double number, String messagePattern, Object... arguments) {
        if (!(Math.signum(number) > 0 && Math.floor(number) == number)) {
            exception(messagePattern, arguments);
        }
    }

    private static void exception(String messagePattern, Object... arguments) {
        throw new RuntimeApplicationException(arguments.length == 0 ? messagePattern : new MessageFormat(messagePattern).format(arguments));
    }

    public static void isEntityFound(boolean isFound, String messagePattern, Object... arguments) {
        if (!isFound)
            throw new NotFoundException(arguments.length == 0 ? messagePattern : new MessageFormat(messagePattern).format(arguments));
    }

}