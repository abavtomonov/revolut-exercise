package controller;

import dao.TransactionDao;
import model.Transaction;
import service.TransactionService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TransactionController {
    @Inject
    private TransactionService transactionService;
    @Inject
    private TransactionDao transactionDao;

    @POST
    @Path("transactions")
    public Transaction create(Transaction transaction) {
        return transactionService.create(transaction);
    }

    @GET
    @Path("transactions/{id}")
    public Transaction get(@PathParam("id") Long id) {
        return transactionService.get(id);
    }

    @GET
    @Path("transactions")
    public Collection<Transaction> getAll() {
        return transactionDao.getAll();
    }


}
