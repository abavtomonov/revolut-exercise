package controller;

import dao.UserDao;
import dto.UserDto;
import model.Account;
import model.User;
import service.UserService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserController {
    @Inject
    private UserDao userDao;
    @Inject
    private UserService userService;

    @GET
    @Path("users/{id}")
    public UserDto get(@PathParam("id") Long id) {
        User user = userService.getById(id);
        List<Account> userAccounts = userService.findAccountsForUserId(user.getId());

        return new UserDto(user, userAccounts);
    }

    @POST
    @Path("users/{id}")
    public UserDto update(@PathParam("id") Long id, User user) {
        User updatedUser = userService.update(new User(id, user));
        List<Account> userAccounts = userService.findAccountsForUserId(user.getId());

        return new UserDto(updatedUser, userAccounts);
    }

    @DELETE
    @Path("users/{id}")
    public void delete(@PathParam("id") Long key) {
        userService.delete(key);
    }

    @DELETE
    @Path("users")
    public void deleteAll() {
        userDao.deleteAll();
    }

    @POST
    @Path("users")
    public UserDto create(User user) {
        User createdUser = userService.create(user);
        return new UserDto(createdUser, Collections.emptyList());
    }

    @GET
    @Path("users")
    public Collection<UserDto> getAll() {
        Stream<User> userStream = userDao.getAll().stream();

        return userStream.map(user -> {
            List<Account> userAccounts = userService.findAccountsForUserId(user.getId());
            return new UserDto(user, userAccounts);
        }).collect(Collectors.toList());
    }
}
