package controller;

import dao.AccountDao;
import model.Account;
import service.AccountService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AccountController {

    @Inject
    private AccountDao accountDao;
    @Inject
    private AccountService accountService;

    @GET
    @Path("accounts/{id}")
    public Account get(@PathParam("id") Long id) {
        return accountDao.getById(id);
    }

    @POST
    @Path("accounts/{id}")
    public Account update(@PathParam("id") Long id, Account entity) {
        return accountService.update(new Account(id, entity));
    }

    @DELETE
    @Path("accounts/{id}")
    public void delete(@PathParam("id") Long key) {
        accountService.delete(key);
    }

    @DELETE
    @Path("accounts")
    public void deleteAll() {
        accountDao.deleteAll();
    }

    @POST
    @Path("accounts")
    public Account create(Account account) {
        return accountService.create(account);
    }

    @GET
    @Path("accounts")
    public Collection<Account> getAll() {
        return accountDao.getAll();
    }

}
