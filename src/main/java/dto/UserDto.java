package dto;

import model.Account;
import model.User;

import java.util.List;

public class UserDto {
    private User user;
    private List<Account> accounts;

    public UserDto() {
    }

    public UserDto(User user, List<Account> accounts) {
        this.user = user;
        this.accounts = accounts;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}
