package dto;

public class ExceptionDto {
    private String message;
    private String className;
    private String errorMessage;
    private long timeStamp;

    public ExceptionDto() {
    }

    public ExceptionDto(String message, String className, String errorMessage, long timeStamp) {
        this.message = message;
        this.className = className;
        this.errorMessage = errorMessage;
        this.timeStamp = timeStamp;
    }
}
