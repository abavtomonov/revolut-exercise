package utils;

import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.Binder;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.junit.After;
import org.junit.Before;
import run.AppInjector;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AbstractInjectionTest {
    private final List<Binder> binders;
    private ServiceLocator locator;

    public AbstractInjectionTest() {
        this.binders = Collections.singletonList(new AppInjector());
    }

    public AbstractInjectionTest(Binder... binders) {
        this.binders = Arrays.asList(binders);
    }

    public AbstractInjectionTest(Binder binder) {
        this.binders = Collections.singletonList(binder);
    }

    @Before
    public void setUp() throws Exception {
        Binder[] objects = binders.toArray(new Binder[0]);
        locator = ServiceLocatorUtilities.bind(objects);
        locator.inject(this);
    }

    @After
    public void tearDown() throws Exception {
        locator.shutdown();

    }
}
