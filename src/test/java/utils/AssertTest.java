package utils;

import exception.RuntimeApplicationException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AssertTest {

    @Test
    public void checkNotNullTest() {
        Assert.checkNotNull(new Object(), "argument is not null");
    }

    @Test
    public void checkNullExceptionMessageTest() {
        String msg = "argument is null";
        try {
            Assert.checkNotNull(null, msg);
        } catch (RuntimeApplicationException e) {
            assertEquals(e.getMessage(), msg);
        }
    }

    @Test
    public void checkIsTrue() throws Exception {
        Assert.checkIsTrue(true, "ok");
    }

    @Test(expected = RuntimeApplicationException.class)
    public void checkIsNotTrue() throws Exception {
        Assert.checkIsTrue(false, "");
    }

    @Test
    public void checkNaturalNumber() throws Exception {
        Assert.checkNaturalNumber(1, "");
    }

    @Test(expected = RuntimeApplicationException.class)
    public void checkNotNaturalNumber() throws Exception {
        Assert.checkNaturalNumber(1.5, "");
    }

    @Test(expected = RuntimeApplicationException.class)
    public void checkNotNaturalNumberZero() throws Exception {
        Assert.checkNaturalNumber(0, "");
    }

    @Test(expected = RuntimeApplicationException.class)
    public void checkNotNaturalNumberNegative() throws Exception {
        Assert.checkNaturalNumber(-1, "");
    }
}
