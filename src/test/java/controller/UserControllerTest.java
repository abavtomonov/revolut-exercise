package controller;

import dto.UserDto;
import model.User;
import org.junit.Before;
import org.junit.Test;
import utils.AbstractInjectionTest;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UserControllerTest extends AbstractInjectionTest {
    private static final String FIRST_NAME = "first name";
    private static final String MIDDLE_NAME = "middle name";
    private static final String LAST_NAME = "last name";

    @Inject
    private UserController userController;

    private User user;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        user = createUser().getUser();
    }

    @Test
    public void testUserControllerIsNotNull() throws Exception {
        assertNotNull(userController);
    }

    @Test
    public void testCreateUser() throws Exception {
        assertNotNull(user.getId());
        assertEquals(user.getFirstName(), FIRST_NAME);
        assertEquals(user.getMiddleName(), MIDDLE_NAME);
        assertEquals(user.getLastName(), LAST_NAME);
    }

    @Test
    public void testGetUser() throws Exception {
        User retrievedUser = userController.get(user.getId()).getUser();
        assertEquals(user, retrievedUser);
    }

    @Test
    public void testGetAllUsers() throws Exception {
        User user2 = createUser().getUser();
        List<User> retrievedUsers = userController.getAll().stream().map(UserDto::getUser).collect(Collectors.toList());
        assertEquals(retrievedUsers.size(), 2);
        assertEquals(retrievedUsers.get(0), user);
        assertEquals(retrievedUsers.get(1), user2);
    }

    @Test
    public void testUpdateUser() throws Exception {
        String lastName = "NEW last name";
        String middleName = "NEW middle name";
        String firstName = "NEW first name";
        User updatedUser = userController.update(user.getId(), new User(null, firstName, middleName, lastName)).getUser();
        assertEquals(updatedUser, user);
        assertEquals(updatedUser.getFirstName(), firstName);
        assertEquals(updatedUser.getMiddleName(), middleName);
        assertEquals(updatedUser.getLastName(), lastName);
    }

    @Test
    public void testDeleteUser() throws Exception {
        User user2 = createUser().getUser();
        assertEquals(userController.getAll().size(), 2);
        userController.delete(user2.getId());
        assertEquals(userController.getAll().size(), 1);
        assertEquals(userController.get(user.getId()).getUser(), user);
    }

    @Test
    public void testDeleteAllUsers() throws Exception {
        createUser();
        assertEquals(userController.getAll().size(), 2);
        userController.deleteAll();
        assertEquals(userController.getAll().size(), 0);
    }

    private UserDto createUser() {
        return userController.create(new User(null, FIRST_NAME, MIDDLE_NAME, LAST_NAME));
    }


}