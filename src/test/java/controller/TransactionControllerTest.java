package controller;

import junit.framework.TestCase;
import model.Account;
import model.Transaction;
import model.User;
import org.junit.Before;
import org.junit.Test;
import utils.AbstractInjectionTest;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Currency;

import static org.junit.Assert.assertEquals;

public class TransactionControllerTest extends AbstractInjectionTest {
    @Inject
    TransactionController transactionController;
    @Inject
    AccountController accountController;
    @Inject
    UserController userController;

    private static final Long USER_ID = 1L;
    private static final Currency CURRENCY = Currency.getInstance("USD");
    private static final BigDecimal BALANCE = BigDecimal.TEN;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        createUser();
    }

    private Long createUser() {
        return userController.create(new User("F", "M", "L")).getUser().getId();
    }

    @Test
    public void testTransactionControllerIsNotNull() throws Exception {
        TestCase.assertNotNull(transactionController);
    }

    @Test
    public void testDeposit() throws Exception {
        Account account = accountController.create(newDemoAccount());
        BigDecimal depositAmount = BigDecimal.ONE;
        Transaction transaction = new Transaction(null, account.getId(), CURRENCY, depositAmount);
        transactionController.create(transaction);
        Account newAccount = accountController.get(account.getId());
        assertEquals(newAccount.getBalance(), account.getBalance().add(depositAmount));
    }

    @Test
    public void testWithdraw() throws Exception {
        Account account = accountController.create(newDemoAccount());
        BigDecimal withdrawAmount = BigDecimal.ONE;
        Transaction transaction = new Transaction(account.getId(), null, CURRENCY, withdrawAmount);
        transactionController.create(transaction);
        Account newAccount = accountController.get(account.getId());
        assertEquals(newAccount.getBalance(), account.getBalance().subtract(withdrawAmount));
    }

    @Test
    public void testTransfer() throws Exception {
        Account accountFrom = accountController.create(newDemoAccount());
        Account accountTo = accountController.create(newDemoAccount());
        Transaction transaction = new Transaction(accountFrom.getId(), accountTo.getId(), CURRENCY, BigDecimal.ONE);
        transactionController.create(transaction);
        Account newAccountFrom = accountController.get(accountFrom.getId());
        Account newAccountTo = accountController.get(accountTo.getId());
        assertEquals(accountFrom.getBalance().subtract(BigDecimal.ONE), newAccountFrom.getBalance());
        assertEquals(accountTo.getBalance().add(BigDecimal.ONE), newAccountTo.getBalance());
    }

    @Test
    public void testGetAllTransactions() throws Exception {
        assertEquals(transactionController.getAll().size(), 0);
        Account accountFrom = accountController.create(newDemoAccount());
        Account accountTo = accountController.create(newDemoAccount());
        for (int i = 0; i < 5; i++) {
            Transaction transaction = new Transaction(accountFrom.getId(), accountTo.getId(), CURRENCY, BigDecimal.ONE);
            transactionController.create(transaction);
        }
        assertEquals(transactionController.getAll().size(), 5);
    }

    @Test
    public void testGetTransaction() throws Exception {
        assertEquals(transactionController.getAll().size(), 0);
        Account accountFrom = accountController.create(newDemoAccount());
        Account accountTo = accountController.create(newDemoAccount());
        Transaction createdTransaction = new Transaction(accountFrom.getId(), accountTo.getId(), CURRENCY, BigDecimal.ONE);
        Transaction sentTransaction = transactionController.create(createdTransaction);

        Transaction receivedTransaction = transactionController.get(sentTransaction.getId());
        assertEquals(sentTransaction, receivedTransaction);
        assertEquals(transactionController.getAll().size(), 1);
    }

    private Account newDemoAccount() {
        return new Account(USER_ID, CURRENCY, BALANCE);
    }
}