package controller;

import model.Account;
import model.User;
import org.junit.Before;
import org.junit.Test;
import utils.AbstractInjectionTest;

import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Currency;

import static org.junit.Assert.*;

public class AccountControllerTest extends AbstractInjectionTest {
    private static final Currency CURRENCY = Currency.getInstance("USD");
    private static final BigDecimal BALANCE = BigDecimal.TEN.setScale(2, RoundingMode.HALF_UP);
    private Long userId;

    @Inject
    AccountController accountController;
    @Inject
    UserController userController;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        userId = createUser();
    }

    private Long createUser() {
        return userController.create(new User("F", "M", "L")).getUser().getId();
    }

    @Test
    public void testCreateAccount() throws Exception {
        Account account = createDemoAccount();
        assertNotNull(account.getId());
        assertEquals(account.getUserId(), userId);
        assertEquals(account.getCurrency(), CURRENCY);
        assertEquals(account.getBalance(), BALANCE);
    }

    @Test
    public void testAccountControllerIsNotNull() throws Exception {
        assertNotNull(accountController);
    }

    @Test
    public void testUpdateAccount() throws Exception {
        Account account = createDemoAccount();

        BigDecimal newBalance = BigDecimal.ONE.setScale(2, BigDecimal.ROUND_HALF_UP);
        Currency newCurrency = Currency.getInstance("EUR");
        Long newUserId = createUser();

        Account updatedAccount = accountController.update(account.getId(), new Account(null, newUserId, newCurrency, newBalance));

        assertEquals(account, updatedAccount);
        assertEquals(updatedAccount.getBalance(), newBalance);
        assertEquals(updatedAccount.getCurrency(), newCurrency);
        assertEquals(updatedAccount.getUserId(), newUserId);
    }

    @Test
    public void testGetAccount() throws Exception {
        Account demoAccount = createDemoAccount();
        Account foundAccount = accountController.get(demoAccount.getId());
        assertEquals(demoAccount, foundAccount);
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteAccount() throws Exception {
        Account demoAccount = createDemoAccount();
        accountController.delete(demoAccount.getId());
        accountController.get(demoAccount.getId());
    }


    @Test
    public void testGetAllAccounts() throws Exception {
        Account demoAccount = createDemoAccount();
        Account demoAccount2 = createDemoAccount();
        ArrayList<Account> accounts = new ArrayList<>(accountController.getAll());
        assertEquals(accounts.size(), 2);
        assertEquals(demoAccount, accounts.get(0));
        assertEquals(demoAccount2, accounts.get(1));
    }

    @Test
    public void testDeleteAllAccounts() throws Exception {
        createDemoAccount();
        createDemoAccount();
        assertEquals(accountController.getAll().size(), 2);
        accountController.deleteAll();
        assertEquals(accountController.getAll().size(), 0);
    }

    private Account createDemoAccount() {
        return accountController.create(new Account(userId, CURRENCY, BALANCE));
    }
}
