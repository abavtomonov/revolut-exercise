package smoke;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import run.Main;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class SimpleSmokeTest {

    private Client client;

    @Before
    public void setUp() throws Exception {
        Main.startServer(3000);
        client = ClientBuilder.newClient();
    }

    @After
    public void tearDown() throws Exception {
        Main.shutdownServer();
    }

    @Test
    public void testIsWadlOpening() throws Exception {
        Response response = client.target(Main.getWadlUrl()).request().get();
        assertEquals(response.getStatus(), Response.Status.OK.getStatusCode());
    }
}
