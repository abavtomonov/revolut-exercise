package model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {

    private static final Long BASE_ID = 1L;
    private static final String BASE_FIRST_NAME = "BASE_FIRST_NAME";
    private static final String BASE_MIDDLE_NAME = "BASE_MIDDLE_NAME";
    private static final String BASE_LAST_NAME = "BASE_LAST_NAME";

    private User user;

    @Before
    public void setUp() throws Exception {
        user = createUser(BASE_ID);
    }

    private User createUser(Long id) {
        return new User(id, BASE_FIRST_NAME, BASE_MIDDLE_NAME, BASE_LAST_NAME);
    }

    @Test
    public void testUserCreation() throws Exception {
        assertEquals(user.getId(), BASE_ID);
        assertEquals(user.getFirstName(), BASE_FIRST_NAME);
        assertEquals(user.getMiddleName(), BASE_MIDDLE_NAME);
        assertEquals(user.getLastName(), BASE_LAST_NAME);
    }

    @Test
    public void testUserEquals() {
        assertTrue(user.equals(createUser(BASE_ID)));
        assertFalse(user.equals(createUser(BASE_ID + 1)));
    }

    @Test
    public void testUserHashCode() throws Exception {
        assertEquals(user.hashCode(), createUser(BASE_ID).hashCode());
        assertNotEquals(user.hashCode(), createUser(BASE_ID + 1).hashCode());
    }
}
