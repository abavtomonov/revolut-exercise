package model;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.junit.Assert.*;

public class AccountTest {

    private static final Long BASE_ACCOUNT_ID = 1L;
    private static final Currency BASE_CURRENCY = Currency.getInstance("USD");
    private static final BigDecimal BASE_BALANCE = BigDecimal.ONE.setScale(2, BigDecimal.ROUND_HALF_UP);
    private static final Long BASE_USER_ID = 1L;

    private Account account;

    @Before
    public void setUp() {
        account = createAccount(1L);
    }

    private Account createAccount(long id) {
        return new Account(id, BASE_USER_ID, BASE_CURRENCY, BASE_BALANCE);
    }

    @Test
    public void testAccountCreation() throws Exception {
        assertEquals(account.getBalance(), BASE_BALANCE);
        assertEquals(account.getCurrency(), BASE_CURRENCY);
        assertEquals(account.getId(), BASE_ACCOUNT_ID);
        assertEquals(account.getUserId(), BASE_USER_ID);
    }

    @Test
    public void testAccountEquals() {
        assertTrue(account.equals(createAccount(BASE_ACCOUNT_ID)));
        assertFalse(account.equals(createAccount(BASE_ACCOUNT_ID + 1)));
    }

    @Test
    public void testAccountHashCode() throws Exception {
        assertEquals(account.hashCode(), createAccount(BASE_ACCOUNT_ID).hashCode());
        assertNotEquals(account.hashCode(), createAccount(BASE_ACCOUNT_ID + 1).hashCode());
    }
}
