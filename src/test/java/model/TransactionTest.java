package model;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

import static org.junit.Assert.*;

public class TransactionTest {
    private static final Long BASE_ID = 1L;
    private static final Long BASE_FROM_ID = 1L;
    private static final Long BASE_TO_ID = 2L;
    private static final Currency BASE_CURRENCY = Currency.getInstance("USD");
    private static final BigDecimal BASE_AMOUNT = BigDecimal.ONE;
    private static final Date BASE_EXECUTION_DATE = new Date();


    private Transaction createTransaction(long transactionId) {
        return new Transaction(transactionId, BASE_FROM_ID, BASE_TO_ID, BASE_CURRENCY, BASE_AMOUNT, BASE_EXECUTION_DATE);
    }


    @Test
    public void testTransactionCreation() throws Exception {
        Transaction transaction = createTransaction(BASE_ID);

        Assert.assertEquals(transaction.getId(), BASE_ID);
        Assert.assertEquals(transaction.getFromAccountId(), BASE_FROM_ID);
        Assert.assertEquals(transaction.getToAccountId(), BASE_TO_ID);
        Assert.assertEquals(transaction.getAmount(), BASE_AMOUNT);
        Assert.assertEquals(transaction.getCurrency(), BASE_CURRENCY);
        Assert.assertEquals(transaction.getExecutionDate(), BASE_EXECUTION_DATE);
    }

    @Test
    public void testAccountEquals() {
        Transaction transaction = createTransaction(BASE_ID);
        assertTrue(transaction.equals(createTransaction(BASE_ID)));
        assertFalse(transaction.equals(createTransaction(BASE_ID + 1)));
    }

    @Test
    public void testAccountHashCode() throws Exception {
        Transaction transaction = createTransaction(BASE_ID);
        assertEquals(transaction.hashCode(), createTransaction(BASE_ID).hashCode());
        assertNotEquals(transaction.hashCode(), createTransaction(BASE_ID + 1).hashCode());
    }
}
