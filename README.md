### Revolut testing exercise. ###
Account money transfer web service.

## How to start ##
* Execute main method of run.Main class
* After the application is started WADL will be available at : **http://localhost:9998/api/application.wadl**

By default the application will start at port 9998. Another port can be set by an optional main parameter.

## Entities ##
Service manages 3 entities :
# User #
URL : api/users

```
#!json

{
    "id": 1,
    "firstName": "Andrey",
    "middleName": "Borisovich",
    "lastName": "Avtomonov"
  }
```

# Account #
URL : api/accounts
```
#!json

{
  "id": 1,
  "userId": 1,
  "currency": "USD",
  "balance": 100
}
```
# Transaction #
URL : api/transactions

```
#!json

{
  "id": 1,
  "fromAccountId": 1,
  "toAccountId": 2,
  "currency": "EUR",
  "amount": 10,
  "executionDate": 1460454243225
}
```

Please refer to WADL for available methods information.


```
#!text

Andrey Avtomonov
andreybavt@gmail.com
```